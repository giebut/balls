﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Game {
    public class Space : MonoBehaviour {
        const float gravity = 1f; // 6.674e-11

        public static Space instance { get; private set; }

        [Header("Parameters")]
        public int absorbtionLimit;
        public int maxNumber;
        public float duration;
        public float offset;
        public float maxVelocityValue;
        [Range(1f, 10f)]
        public float nearRatio = 1f;
        [Range(1f, 100f)]
        public float farRatio = 100f;

        [Header("References")]
        public Camera camera;
        public Text numberText;

        [Header("Prefabs")]
        public Ball ball;

        float time;
        float near;
        float far;
        List<Ball> pool;

        public bool isBelowMaxNumber { get { return pool.Count < maxNumber; } }

        void Awake() {
            if(instance == null) {
                instance = this;
            }

            pool = new List<Ball>();
        }

        void Start() {
            time = 0f;
            near = nearRatio * camera.nearClipPlane + offset;
            far = (nearRatio < farRatio ? farRatio : nearRatio) * camera.nearClipPlane + offset;
        }

        void Update() {
            if(time < 0f && isBelowMaxNumber) {
                generateBall();

                time = duration;
            }

            time -= Time.deltaTime;
        }

        void generateBall() {
            var clone = Instantiate(ball);

            var position = new Vector3() {
                x = Random.Range(0f, Screen.width),
                y = Random.Range(0f, Screen.height),
                z = Random.Range(near, far)
            };

            clone.transform.position = camera.ScreenToWorldPoint(position);

            pool.Add(clone);

            numberText.text = pool.Count.ToString();
        }

        public Vector3 getForce(Ball ball) {
            var force = Vector3.zero;

            for(int i = 0; i < pool.Count; i++) {
                if(pool[i].gameObject.activeSelf && pool[i] != ball) {
                    var direction = pool[i].transform.position - ball.transform.position;

                    if(direction.sqrMagnitude > 0f) {
                        float value = gravity * pool[i].mass * ball.mass / (direction.sqrMagnitude);

                        force += value * direction;
                    }
                }
            }

            return isBelowMaxNumber ? force : -force;
        }

        public Vector3 randomizeVelocity() {
            var direction = new Vector3() {
                x = Random.value - 0.5f,
                y = Random.value - 0.5f,
                z = Random.value - 0.5f
            };

            return Random.Range(0f, maxVelocityValue) * direction.normalized;
        } 
    }
}