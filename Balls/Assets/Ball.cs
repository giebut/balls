﻿using System.Collections.Generic;
using UnityEngine;

namespace Game {
    [RequireComponent(typeof(Rigidbody))]
    [RequireComponent(typeof(SphereCollider))]
    public class Ball : MonoBehaviour {
        static readonly Vector3 defaultScale = 2f * Vector3.one;

        static Vector3 direction;

        public float immunityTime;
        public Transform sphere;

        float defaultMass;
        float defaultRadius;
        float time;
        Rigidbody rigidbody;
        SphereCollider sphereCollider;
        List<Ball> absorbedBalls;

        public float mass {
            get { return rigidbody.mass; }
        }

        void Awake() {
            rigidbody = GetComponent<Rigidbody>();
            sphereCollider = GetComponent<SphereCollider>();
            absorbedBalls = new List<Ball>();

            defaultMass = rigidbody.mass;
            defaultRadius = sphereCollider.radius;
        }

        void OnEnable() {
            sphere.localScale = Vector3.one;

            rigidbody.mass = defaultMass;
            sphereCollider.radius = defaultRadius;
        }

        void Update() {
            rigidbody.AddForce(Space.instance.getForce(this));
        }

        void FixedUpdate() {
            time -= Time.fixedDeltaTime;

            sphereCollider.enabled = time <= 0f;
        }

        void OnCollisionStay(Collision collision) {
            if(time < 0f && gameObject.activeSelf && Space.instance.isBelowMaxNumber) {
                var ball = collision.collider.GetComponent<Ball>();

                if(ball != null) {
                    absorb(ball);
                }
            }
        }

        void absorb(Ball ball) {
            transform.position = sphereCollider.radius > ball.sphereCollider.radius ? transform.position : ball.transform.position;

            // promień wyznaczony z założenia, że S = S1 + S2 gdzie S oznacza pole powierzchni sfery
            sphereCollider.radius = Mathf.Sqrt(sphereCollider.radius * sphereCollider.radius + ball.sphereCollider.radius * ball.sphereCollider.radius);
            sphere.transform.localScale = sphereCollider.radius * defaultScale;

            rigidbody.mass += ball.rigidbody.mass;

            absorbedBalls.Add(ball);

            while(ball.absorbedBalls.Count > 0) {
                absorbedBalls.Add(ball.absorbedBalls[0]);
                ball.absorbedBalls.RemoveAt(0);
            }

            ball.gameObject.SetActive(false);

            if(absorbedBalls.Count + 1 > Space.instance.absorbtionLimit) { // zwiększam o jeden, bo aktualny obiekt również trzeba wliczyć
                explode();
            }
        }

        public void explode() {
            OnEnable();

            time = immunityTime;
            sphereCollider.enabled = false;
            transform.position = transform.position;
            rigidbody.velocity = Space.instance.randomizeVelocity();

            while(absorbedBalls.Count > 0) {
                absorbedBalls[0].time = immunityTime;
                absorbedBalls[0].sphereCollider.enabled = false;
                absorbedBalls[0].transform.position = transform.position;
                absorbedBalls[0].rigidbody.velocity = Space.instance.randomizeVelocity();
                absorbedBalls[0].gameObject.SetActive(true);
                absorbedBalls.RemoveAt(0);
            }
        }
    }
}